# gitlab-runner-config

This is a sidecar/helper container that generates `config.toml` from environment
variables for a `gitlab/gitlab-runner` container.
