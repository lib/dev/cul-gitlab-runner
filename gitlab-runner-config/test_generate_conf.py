import base64
import json
import tomllib
from pathlib import Path
from subprocess import check_call, check_output
from typing import Generator, Literal

import pytest
from gitlab_runner_config import ConfigSettings, docker_auth_config, generate_conf


@pytest.fixture
def test_secrets_dir(tmp_path: Path) -> Path:
    path = tmp_path / "secrets"
    path.mkdir()
    return path


@pytest.fixture
def override_secrets_dir(test_secrets_dir: Path) -> Generator[Path, None, None]:
    og_secrets_dir = ConfigSettings.model_config.get("secrets_dir")
    ConfigSettings.model_config["secrets_dir"] = str(test_secrets_dir)
    try:
        yield test_secrets_dir
    finally:
        ConfigSettings.model_config["secrets_dir"] = og_secrets_dir


@pytest.fixture
def config_envar_values() -> dict[str, str]:
    return {
        "concurrent_jobs": "42",
        "check_interval": "2",
        "cam_gitlab_url": "https://gitlab.example.cam.ac.uk/",
        "name": "gitlab-ci-foo",
        "token": "secret0",
        "cam_gitlab_container_registry_host": "registry.example",
        "cam_gitlab_container_registry_token": "secret1",
        "cache_minio_host": "example-minio",
        "cache_minio_port": "1234",
        "cache_minio_access_key": "keyname",
        "cache_minio_secret_key": "secret2",
        "cache_minio_bucket_name": "example-cache",
        "cache_minio_insecure": "true",
        "docker_default_job_image": "someimage:1.2.3",
        "docker_volumes": '["/foo","bar:/baz"]',
        "docker_runtime": "runc",
        "job_environment": '["FOO=BAR","BAZ=BOZ"]',
    }


@pytest.fixture(params=["env", "file-env"])
def config_envars(
    config_envar_values: dict[str, str],
    monkeypatch: pytest.MonkeyPatch,
    request: pytest.FixtureRequest,
    override_secrets_dir: Path,
) -> None:
    var_type: Literal["env", "file-env"] = request.param
    if var_type == "env":
        for name, value in config_envar_values.items():
            monkeypatch.setenv(f"GITLAB_CI_RUNNER_{name.upper()}", value)
    else:
        assert var_type == "file-env"
        for name, value in config_envar_values.items():
            var_file = override_secrets_dir / name
            var_file.write_text(value)
            monkeypatch.setenv(f"GITLAB_CI_RUNNER_{name.upper()}_FILE", str(var_file))


@pytest.mark.usefixtures("config_envars")
def test_generate_conf() -> None:
    settings = ConfigSettings()  # type: ignore[call-arg]

    config = generate_conf(settings)
    assert config["concurrent"] == 42
    assert config["check_interval"] == 2
    assert config["runners"][0]["executor"] == "docker"
    assert config["runners"][0]["name"] == "gitlab-ci-foo"
    assert config["runners"][0]["url"] == "https://gitlab.example.cam.ac.uk/"
    assert config["runners"][0]["token"] == "secret0"

    docker_auth_config_kv = config["runners"][0]["environment"][0].split(
        "=", maxsplit=1
    )
    assert docker_auth_config_kv[0] == "DOCKER_AUTH_CONFIG"
    assert docker_auth_config_kv[1] == docker_auth_config(
        registry_address="registry.example", auth_token="secret1"
    )
    assert config["runners"][0]["environment"][1:] == ["FOO=BAR", "BAZ=BOZ"]

    assert config["runners"][0]["cache"]["Type"] == "s3"
    assert config["runners"][0]["cache"]["s3"]["ServerAddress"] == "example-minio:1234"
    assert config["runners"][0]["cache"]["s3"]["AccessKey"] == "keyname"
    assert config["runners"][0]["cache"]["s3"]["SecretKey"] == "secret2"
    assert config["runners"][0]["cache"]["s3"]["BucketName"] == "example-cache"
    assert config["runners"][0]["cache"]["s3"]["Insecure"] == True

    assert config["runners"][0]["docker"]["image"] == "someimage:1.2.3"
    assert config["runners"][0]["docker"]["volumes"] == ["/foo", "bar:/baz"]
    assert config["runners"][0]["docker"]["runtime"] == "runc"

    assert "example-minio:host-gateway" in config["runners"][0]["docker"]["extra_hosts"]


@pytest.mark.usefixtures("config_envars")
def test_cli_stdout() -> None:
    stdout = check_output("generate-gitlab-runner-config")
    settings = ConfigSettings()  # type: ignore[call-arg]

    config = tomllib.loads(stdout.decode())
    assert config == generate_conf(settings)


@pytest.mark.usefixtures("config_envars")
def test_cli_write_file(tmp_path: Path) -> None:
    config_file = tmp_path / "config.toml"
    status = check_call(
        [
            "generate-gitlab-runner-config",
            "--write-config-file",
            "--config-file-path",
            str(config_file),
        ]
    )
    assert status == 0
    settings = ConfigSettings()  # type: ignore[call-arg]

    config = tomllib.loads(config_file.read_text())
    assert config == generate_conf(settings)


def test_docker_auth_config() -> None:
    auth = docker_auth_config(registry_address="registry.example.com", auth_token="abc")

    decoded_auth = json.loads(auth)
    auth_str = decoded_auth["auths"]["registry.example.com"]["auth"]
    assert base64.decodebytes(auth_str.encode()) == b"token:abc"
