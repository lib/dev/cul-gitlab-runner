"""Generate a gitlab-runner conf file from envars.
"""

import base64
import json
import os
import sys
import traceback
from json import JSONDecodeError
from pathlib import Path
from typing import Any, Callable, Final, Mapping, Sequence, Type

import click
import tomli_w
from pydantic import Field, HttpUrl, PositiveInt, ValidationError
from pydantic.fields import FieldInfo
from pydantic_settings import (
    BaseSettings,
    PydanticBaseSettingsSource,
    SettingsConfigDict,
)
from pydantic_settings.sources import SettingsError
from pydantic_settings_file_envar import FileSuffixEnvSettingsSource

envar_prefix = "GITLAB_CI_RUNNER_"


class ConfigSettings(BaseSettings):
    concurrent_jobs: PositiveInt = Field(default=12)
    check_interval: PositiveInt = Field(default=3)
    cam_gitlab_url: HttpUrl = Field(default="https://gitlab.developers.cam.ac.uk/")
    name: str = Field(default="gitlab-ci-1")
    token: str
    cam_gitlab_container_registry_host: str = Field(
        default="registry.gitlab.developers.cam.ac.uk"
    )
    cam_gitlab_container_registry_token: str
    cache_minio_host: str = Field(default="gitlab-ci-minio")
    cache_minio_port: PositiveInt = Field(default=9005)
    cache_minio_access_key: str
    cache_minio_secret_key: str
    cache_minio_bucket_name: str = Field(default="runner-cache")
    cache_minio_insecure: bool = Field(default=False)
    docker_default_job_image: str = Field(default="ubuntu:latest")
    docker_volumes: Sequence[str] = Field(default_factory=list)
    docker_runtime: str | None = Field(default=None)
    job_environment: Sequence[str] | None = Field(default=None)

    model_config = SettingsConfigDict(
        env_prefix=envar_prefix, secrets_dir="/run/secrets"
    )

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return (
            init_settings,
            env_settings,
            # load _FILE suffix envars from files
            FileSuffixEnvSettingsSource(settings_cls),
        )


def docker_auth_config(*, registry_address: str, auth_token: str) -> str:
    config = {
        "auths": {
            registry_address: {
                "auth": base64.urlsafe_b64encode(
                    f"token:{auth_token}".encode()
                ).decode()
            }
        }
    }
    return json.dumps(config, separators=(",", ":"))


def generate_conf(settings: ConfigSettings) -> dict[str, Any]:
    return {
        "concurrent": settings.concurrent_jobs,
        "check_interval": settings.check_interval,
        "session_server": {"session_timeout": 1800},
        "runners": [
            {
                "name": settings.name,
                "url": str(settings.cam_gitlab_url),
                "token": settings.token,
                "shell": "bash",
                "executor": "docker",
                "environment": [
                    f"DOCKER_AUTH_CONFIG={docker_auth_config(registry_address=settings.cam_gitlab_container_registry_host, auth_token=settings.cam_gitlab_container_registry_token)}",
                    *(settings.job_environment or []),
                ],
                "feature_flags": {"FF_NETWORK_PER_BUILD": True},
                "custom_build_dir": {},
                "cache": {
                    "Type": "s3",
                    "Path": "default",
                    "Shared": True,
                    "s3": {
                        "ServerAddress": f"{settings.cache_minio_host}:{settings.cache_minio_port}",
                        "AccessKey": settings.cache_minio_access_key,
                        "SecretKey": settings.cache_minio_secret_key,
                        "BucketName": settings.cache_minio_bucket_name,
                        "Insecure": settings.cache_minio_insecure,
                    },
                    "gcs": {},
                    "azure": {},
                },
                "docker": dict_without_none_values(
                    tls_verify=True,
                    image=settings.docker_default_job_image,
                    privileged=False,
                    disable_entrypoint_overwrite=False,
                    oom_kill_disable=False,
                    disable_cache=False,
                    shm_size=0,
                    extra_hosts=[f"{settings.cache_minio_host}:host-gateway"],
                    volumes=settings.docker_volumes,
                    runtime=settings.docker_runtime,
                ),
            }
        ],
    }


def dict_without_none_values(**entries: Any) -> Mapping[str, Any]:
    return {k: v for (k, v) in entries.items() if v is not None}


default_config_path: Final = "/etc/gitlab-runner/config.toml"


class CliSettings(BaseSettings):
    write_config_file: bool = Field(default=False)
    config_file_path: str = Field(default=default_config_path)

    model_config = SettingsConfigDict(env_prefix=envar_prefix)


def generate_help(main: Callable[..., None]) -> Callable[..., None]:
    config_options = "\n".join(
        f" - {envar_prefix}{field.upper()}"
        for field in ConfigSettings.model_fields.keys()
    )

    main.__doc__ = f"""\
Generate a gitlab-runner config.toml file from envars.

The config prints to stdout unless --write-config-file is set. The default
config path is {default_config_path}.

CLI options can also be set via {envar_prefix} envars.

\b
The configuration options are:
{config_options}
"""
    return main


@click.command()
@click.option("--write-config-file/--no-write-config-file", default=None)
@click.option("--config-file-path")
@generate_help
def main(write_config_file: bool | None, config_file_path: str | None) -> None:
    provided_cli_options: dict[str, Any] = {
        k: v
        for (k, v) in dict(
            write_config_file=write_config_file, config_file_path=config_file_path
        ).items()
        if v is not None
    }
    cli_settings = CliSettings(**provided_cli_options)

    try:
        config = ConfigSettings()  # type: ignore[call-arg]
    except SettingsError as e:
        if not isinstance(e.__cause__, JSONDecodeError):
            raise e

        json_err = e.__cause__
        print(
            f"Unable to load config values to insert into the config: {e}: "
            f"{type(json_err).__name__}: {json_err}",
            file=sys.stderr,
        )
        sys.exit(1)
    except ValidationError as e:
        print(
            f"""\
Unable to load config values to insert into the config: {e}",

Hint: Settings must be provided using upper-case environment variables, \
starting with {envar_prefix}. For example, the `token` setting is \
{envar_prefix}TOKEN"\
""",
            file=sys.stderr,
        )
        sys.exit(1)

    generated_config = tomli_w.dumps(generate_conf(config))
    if cli_settings.write_config_file:
        Path(cli_settings.config_file_path).write_text(generated_config)
    else:
        print(generated_config, end="")
