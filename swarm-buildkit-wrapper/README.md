# swarm-buildkit-wrapper

This container image runs a buildkit container from a Docker Swarm service. It's
a workaround for Swarm's lack of support for `--privileged` or `--security-opt`
on swarm services, which are required to run buildkit (rootless buildkit needs `--security-opt`).

The container expects to have the host's docker socket mounted into it, and when
started, it'll run a buildkit container (using regular docker CLI) and block until it exits.
