#!/usr/bin/env bash
set -euo pipefail

buildkit_socket_volume="${SWARM_BUILDKIT_WRAPPER_SOCKET_VOLUME:-buildkit-socket}"
buildkit_state_volume="${SWARM_BUILDKIT_WRAPPER_STATE_VOLUME:-buildkit-state}"
buildkit_tag="${SWARM_BUILDKIT_WRAPPER_TAG:-latest}"

raw_running_containers=$(docker container ls \
    --filter=label=uk.ac.cam.lib.buildkit-swarm-wrapper.managed=true \
    --format='{{.ID}}'
)
if [[ ${raw_running_containers?} != "" ]]; then
    readarray <<<"${raw_running_containers?}" -t running_containers
    echo "Stopping existing container(s): ${running_containers[*]}" >&2
    docker container stop "${running_containers[@]}"
fi

container_id=$(docker container run --detach \
    --label=uk.ac.cam.lib.buildkit-swarm-wrapper.managed=true \
    --privileged \
    -v "${buildkit_socket_volume:?}:/run/buildkit" \
    -v "${buildkit_state_volume:?}:/var/lib/buildkit" \
    --name "swarm-buildkit-wrapper.$(hostname).buildkit" \
    "moby/buildkit:${buildkit_tag:?}"
)

function handle_stop() {
    echo "Stopping buildkit container: ${container_id:?}" >&2
    docker container stop "${container_id:?}"
    exit 0
}

trap handle_stop SIGTERM SIGINT

echo -n "Started buildkit container: " >&2
echo "${container_id:?}"

docker container wait "${container_id:?}" >&2 &
wait_pid=$!
wait "${wait_pid:?}"
