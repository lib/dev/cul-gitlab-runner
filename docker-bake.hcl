variable "BAKE_PUSH" {
  default = ""
}

variable "CI_COMMIT_BRANCH" {
    default = "dev"
}

repo = "registry.gitlab.developers.cam.ac.uk/lib/dev/cul-gitlab-runner"

function "pushable_image_output" {
    params = []
    result = BAKE_PUSH == "true" ? ["type=registry"] : []
}

function "tags" {
    params = []
    result = flatten([
        CI_COMMIT_BRANCH == "main" ? ["latest"] : [],
        "branch-${CI_COMMIT_BRANCH}"
    ])
}

group "default" {
    targets = [
        "gitlab-runner-config",
        "test-gitlab-runner-config",
        "ci-docker-remote-buildkit",
        "swarm-buildkit-wrapper",
    ]
}

target "base" {
    output = pushable_image_output()
}

target "gitlab-runner-config" {
    context = "gitlab-runner-config"
    target = "gitlab-runner-config"
    tags = formatlist("${repo}/gitlab-runner-config:%s", tags())
    output = pushable_image_output()
}

target "test-gitlab-runner-config" {
    context = "gitlab-runner-config"
    target = "smoke-test"
    // Always run tests
    no-cache-filter = ["smoke-test"]
    output = []
}

target "ci-docker-remote-buildkit" {
    context = "ci-docker-remote-buildkit"
    target = "main"
    tags = formatlist("${repo}/ci-docker-remote-buildkit:%s", tags())
    output = pushable_image_output()
}

target "swarm-buildkit-wrapper" {
    context = "swarm-buildkit-wrapper"
    tags = formatlist("${repo}/swarm-buildkit-wrapper:%s", tags())
    output = pushable_image_output()
}
