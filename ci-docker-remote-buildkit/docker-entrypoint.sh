#!/usr/bin/env bash
set -euo pipefail

if [[ ${CI_REGISTRY:-} && ${CI_REGISTRY_USER:-} && ${CI_REGISTRY_PASSWORD:-} ]]; then
    echo "Logging in to ${CI_REGISTRY@Q}" >&2
    docker <<<"${CI_REGISTRY_PASSWORD:?}" login --password-stdin -u \
        "${CI_REGISTRY_USER:?}" "${CI_REGISTRY:?}"
fi

if [[ "${DOCKERHUB_ACCESS_TOKEN:-}" ]]; then
    echo "Logging in to dockerhub" >&2
    docker <<<"${DOCKERHUB_ACCESS_TOKEN:?}" login --password-stdin \
        -u "${DOCKERHUB_ACCESS_TOKEN_USER:-cudladmin}"
fi

if [[ ${BUILDKIT_HOST:-} ]]; then
    echo "Creating remote builder for BUILDKIT_HOST=${BUILDKIT_HOST@Q}" >&2
    docker buildx create --driver=remote --use
fi

function start_ssh_agent() {
    echo "Starting ssh-agent with ${1:?}" >&2
    ssh_agent_env=$(ssh-agent)
    eval "${ssh_agent_env:?}"
}

if [[ "${CI_SSH_KEY:-}" ]]; then
    # Decode base64-encoded keys — gitlab variables can be masked in log output
    # if they're only base64 characters.
    if [[ "${CI_SSH_KEY:?}" =~ ^[-A-Za-z0-9+/]*={0,3}$ ]]; then
        start_ssh_agent 'CI_SSH_KEY (base64)'
        ssh-add <(base64 -d <<<"${CI_SSH_KEY:?}")
    else
        start_ssh_agent 'CI_SSH_KEY'
        ssh-add - <<<"${CI_SSH_KEY:?}"
    fi
elif [[ "${CI_SSH_KEY_FILE:-}" ]]; then
    start_ssh_agent 'CI_SSH_KEY_FILE'

    if [[ -f "${CI_SSH_KEY_FILE:?}" ]]; then
        # The secret file is world-readable, but must be owner-readable for SSH
        chmod 600 "${CI_SSH_KEY_FILE:?}"
        ssh-add "${CI_SSH_KEY_FILE:?}"
    else
        # GitLab CI runner injects file-type CI variables into the container as
        # regular environment variables, not file paths. I assume it has its own
        # setup logic to write them to files that runs after the entrypoint.
        ssh-add - <<<"${CI_SSH_KEY_FILE:?}"
    fi
fi

exec "$@"
