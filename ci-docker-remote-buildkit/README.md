# ci-docker-remote-buildkit

This is a base image for GitLab CI jobs that use our remote buildkit daemon. It
contains the docker CLI which is auto-configured with a buildx builder using the
buildkit daemon, so jobs can run `docker buildx` commands without additional
setup. See the CI config in this repo for an example.

## Options

### SSH Key

Set the `CI_SSH_KEY` variable to an SSH private key to have the image start an
SSH agent with the key added on startup. You can then use the
[buildx `--ssh`](https://docs.docker.com/engine/reference/commandline/buildx_build/#ssh)
option to let builds access resources protected by the SSH key.

The private keys can be base64-encoded (without newlines), which allows GitLab
to mask them in log output.

Alternatively, use a GitLab file-type CI variable named `CI_SSH_KEY_FILE`.
