# cul-gitlab-runner

This repo contains Docker images and configuration used by the CUL's GitLab CI
runners and jobs that they run.

We deploy the runners using Docker Swarm, using `docker-stack.yml`. (This config
is not deployed directly, rather it's the basis for the actual config deployed
by Puppet.)

We use the official `gitlab/gitlab-runner` image for the runners, but have our
own image to generate their config files from envars. We also use the official
`minio/minio` image to provide S3-compatible object storage for the runner build
cache. We run `moby/buildkit` to provide buildkitd — we use a remote buildx
builder pointing to this in CI jobs.

This repo houses 3 images used by our CI runners/jobs:

- `gitlab-runner-config` — generates config.toml for `gitlab/gitlab-runner`
- `swarm-buildkit-wrapper` — a compatibility shim to run the buildkit container
  in Docker Swarm, as buildkit requires container options that Swarm cannot
  configure by itself.
- `ci-docker-remote-buildkit` — used by CI jobs that use the remote buildkit
  daemon provided by our runner environment. It contains the docker CLI (without
  a daemon) and automatically logs in to the GitLab and dockerhub container
  registries when starting. It also creates and activates a remote buildx
  builder pointing to our buildkit daemon on startup.

  The result is that CI jobs using this image can run `docker buildx ...`
  commands without any setup. See [`.gitlab-ci.yml`](.gitlab-ci.yml) here for an
  example.
